# User Support

## Introduction

The AGS website is designed to support users of the AGS Format. The website address is [*www.ags.org.uk*](http://www.ags.org.uk).

It contains this publication, in Adobe PDF, together with the latest AGS Format abbreviations (ABBR) and other resources to assist those using the format or developing software applications to interpret AGS Format data files. The site also contains a discussion board where users can discuss questions with the AGS Format committee members and other users.

## Discussion Board

The discussion board is an area of the website that has been designed for the support of AGS Format users. If there is any doubt on the use of a particular aspect of the Format, consult the discussion board for previous questions and discussions on the subject.

If an answer cannot be found, place a new discussion on the discussion board. The board is monitored by the AGS Data Management Working Party members.

Full instructions on how to use the discussion board are available on the website.

## Guidance and Example AGS data files

Guidance on the recommended usage of the AGS Format is published on the website.

In addition, example AGS data files are posted on the website. The example files provide practical demonstration of the format.

## AGS Format Abbreviations (ABBR), Units (UNIT) and Data Types (TYPE)

The latest copies of AGS abbreviations, units and data types can be viewed and downloaded from the website.

Users should check the list of ABBR abbreviations before defining a non-standard abbreviation. If the required item is not listed on the website, please submit your suggestion.

All suggestions will be considered and commented on by the AGS Data Management Working Party. Appropriate abbreviations will be added to the website list.

## Downloading This Publication

The document can be downloaded in PDF format from the AGS website free of charge. The document is distributed as shareware and can be read without charge.

## AGS Format Registration

Companies that use the AGS Format to exchange data electronically are required to register with the AGS. A full list of registered companies is available on the website. Registration forms and information on current charges can be downloaded from the website.

If your company receives AGS Format data, ensure that your data producer is registered to use the AGS Format.

## Update Notification

Registered users of the Format will receive news and updates by email.

## Registration Benefits

Registered users of the format will also:

- Be sent the data dictionary in CSV and Excel file formats.

- Be able to use the AGS Data Logo on their reports (see below).

- Be able to download example AGS files and abbreviation lists from the website.

- Be included on the list of registered users.

- Make suggestions for future editions of the AGS Format or additional abbreviations via the discussion forum.

A list of Registered Users is given on the AGS website at [*www.ags.org.uk*](http://www.ags.org.uk).

The AGS wishes to encourage Data Providers to declare on their paper reports when the data contained within the report is also available in AGS Format. This will become of considerable benefit to third party Receivers, who are not the primary Receivers that commissioned the report. To this end the AGS provides the following AGS Format logo to Registered Users of the Format.

<img src="/images/logo.png" alt="AGS Logo" style="height:50px;">

The logo should be used as follows:

1.  The logo should be included in a prominent position on the front cover, or inside front cover, of the Factual Report to indicate that the data contained within the report has also been provided to the primary Receiver in AGS Format.

2.  The logo should also be included on every log or test result sheet within a report, as logs and results sheets are frequently separated from the main text of the report.

3.  The logo is only to be used by Registered Users of the Format.

Note that we intend to retain the logo representing a 3½” floppy disc as homage to the origin of the AGS Format.

## Suggestions for Future Additions

Registered users of the format will be able to suggest additions to the data dictionary via the website. All suggestions will be considered and commented on by the AGS committee and all appropriate suggestions will be included in the next release of the format.
