# FOREWORD

The AGS Data Management Working Party continually monitors the use of the AGS format within the industry and strives to ensure its relevance to current working practice.

In response to new laboratory industry standards and updated UK practice the AGS4 Addendum October 2011 document has been updated and amended and shall be known as “AGS 4.0.4”. Detailed amendments are listed in [Appendix 3](appendix3#appendix-3).

Due to the additional headings in AGS 4.0.4 the DICT group needs to be present in every AGS 4.0.4 submission listing these new headings.

Whilst the Format is used throughout the world, this document is specifically written for use in accordance with UK practice. Guidance notes for its use with other codes and standards are available from the AGS website.

**Jackie Bland**

**Working Party Chair 2010 - present**
