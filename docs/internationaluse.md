# INTERNATIONAL USE OF THE AGS FORMAT

The requirements for other countries to adopt and use the AGS Format as a national data transfer standard are defined in a Guidance Document on the AGS website. The documentation includes the approach to be followed that permits implementation of the AGS Format for transfer in accordance with the technical standards of that country.
