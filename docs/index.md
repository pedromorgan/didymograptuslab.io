# **Electronic Transfer of Geotechnical and  Geoenvironmental Data**




© The Association of Geotechnical and Geoenvironmental Specialists, 2017

Edition 4.0.4 – February 2017


*Although every effort has been made to check the accuracy of the information and validity of the guidance given in this document, neither the members of the Working Party nor the Association of Geotechnical and Geoenvironmental Specialists accept any responsibility for misstatements contained herein or misunderstanding arising here from.*

The copyright notice displayed in this document indicates when the document was last issued.

**Association of Geotechnical and Geoenvironmental Specialists<br>Forum Court,<br>Office 205, Devonshire House Business Centre<br>29-31 Elmfield Road<br>Bromley, Kent<br>BR1 1LT<br>UNITED KINGDOM**

tel.: 020 8658 8212 email: <dataformat@ags.org.uk>  web site: <http://www.ags.org.uk>

<br>
![AGS banner](images/ags-banner.png)