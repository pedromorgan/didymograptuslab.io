# APPENDIX 2

## Data Management

Refer to BS8574:2014 ‘Code of practice for the management of geotechnical data for ground engineering projects’ and the project or organization specific data management plan.

### Transmittal record

The Producer will produce a transmittal record for each submission of data with the minimum following details:

- The heading 'AGS Format Data'

- The project identification (PROJ\_ID)

- The unique issue sequence reference (TRAN\_ISNO)

- The name of the Receiver (TRAN\_RECV)

- The date of issue to the Receiver (TRAN\_DATE)

- The name of the Producer (TRAN\_PROD)

- The AGS Edition Reference (TRAN\_AGS)

A general description of the data transferred and/or a file listing for associated files

In addition the transmittal record will detail the following for each AGS Format data file within the data submission, including all associated files:

- The file name including the extension

- The date of file creation

- The time of file creation

- The file size in bytes

- A general description of the data contained in each file and/or a file listing for associated files

A transmittal record should be prepared for each data set and accompany it each time a data set is issued. An example follows:

### Suggested Format for a Transmittal Record

Insert link to .odt transmittal template





































  ---------------- ----------------------- --------------------------- --------------- -----------------------------------------
