# USER SUPPORT

The AGS has made provision on its website
([*www.ags.org.uk*](http://www.ags.org.uk)) for:

-   Downloading of this document

-   Provision of standard abbreviations

-   Guidance documents

-   Example files

-   Discussion boards

These resources aim to provide users with the appropriate materials to support implementation and use of the AGS Format. The discussion boards assist in identification of user needs and support the development of the format to meet industry requirements. The website is also used to communicate amendments to registered users.

[Appendix 1](appendix1#appendix-1) provides further information on the services provided to AGS Format users via the website.