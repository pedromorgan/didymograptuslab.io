# INTERNAL, PRELIMINARY AND FINAL DATA

The precise use of AGS Format data files within the project process and data management activities is not pre-determined. The data files are structured in order to allow the presentation of preliminary data as well as its updating during the course of a project, prior to issue of the final data.

AGS4 includes new Groups relating to the transmittal of laboratory testing requirements; these data form part of the project process and do not form part of the final data report.

Preliminary data in electronic format can be useful on major projects where design is undertaken during the period of the investigation. However, the need for this facility should be very carefully considered by the receivers before including it in their Contract Specifications since it will require the imposition of rigorous management procedures. The highlighting of change in data is considered to pose significant difficulties and hence preliminary data should be replaced by subsequent data and not merely updated by it. Where the highlighting of change is required, this should be a facility incorporated in the receiver’s software. This does not preclude submission of parts of the data on separate media but the producer must ensure that the data within all separate issues are compatible and that updates are carried through all sub-sets of the data.

Each issue shall be given a unique issue sequence reference. AGS4 includes the new transmittal group, TRAN, to manage this process and include information about the data transfer within the transferred file.

Clear labelling of files and media and conventions for its security and management are vital to the implementation of a practical system. These aspects are dealt with in [Appendix 2](appendix2#appendix-2).

